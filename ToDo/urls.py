"""ToDo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from listToDo import views

urlpatterns = [
    path('admin/', admin.site.urls),

    # Authentication
    path("signup/", views.signupuser, name="signupuser"),
    path("login/", views.loginuser, name="loginuser"),
    path("logout/", views.logoutuser, name="logoutuser"),

    # ToDo
    path("", views.home, name="home"),
    path("todos/", views.currenttodos, name="currenttodos"),
    path("creation/", views.createtodo, name="createtodo"),
    path("complete/", views.completedtodos, name="completedtodos"),
    path("todo/<int:todo_pk>", views.viewtodo, name='viewtodo'),
    path("todo/<int:todo_pk>/complete/", views.completedtodo, name='completedtodo'),
    path("todo/<int:todo_pk>/delete", views.deletetodo, name='deletetodo'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


handler404 = 'listToDo.views.my_custom_page_not_found_view'
handler500 = 'listToDo.views.my_custom_error_view'
handler403 = 'listToDo.views.my_custom_permission_denied_view'
handler400 = 'listToDo.views.my_custom_bad_request_view'


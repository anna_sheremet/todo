from django.db import IntegrityError
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from .forms import TodoForm
from .models import ToDo
from django.utils import timezone
from django.contrib.auth.decorators import login_required

def home(request):
    return render(request, 'listToDo/home.html')


def signupuser(request):
    if request.method == 'GET':
        return render(request, "listToDo/signupuser.html", {'form': UserCreationForm()})
    else:
        if request.POST['password1'] == request.POST['password2']:
            try:
                user = User.objects.create_user(request.POST['username'], password=request.POST['password1'])
                user.save()
                login(request, user)
                return redirect('currenttodos')
            except IntegrityError:
                return render(request, "listToDo/signupuser.html",
                              {'form': UserCreationForm(), 'error': "This name has already been taken. Please. choose "
                                                                    "a new one"})
        else:
            return render(request, "listToDo/signupuser.html",
                          {'form': UserCreationForm(), 'error': 'Password did not match'})


def loginuser(request):
    if request.method == 'GET':
        return render(request, "listToDo/loginuser.html", {'form': AuthenticationForm()})
    else:
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is None:
            return render(request, 'listToDo/loginuser.html',
                          {'form': AuthenticationForm(), 'error': 'Username or password didn`t match'})
        else:
            login(request, user)
            return redirect('currenttodos')

@login_required
def currenttodos(request):
    todos = ToDo.objects.filter(user=request.user, completed__isnull=True)
    return render(request, 'listToDo/current_todos.html', {'todos': todos})

@login_required
def viewtodo(request, todo_pk):
    todo = get_object_or_404(ToDo, pk=todo_pk, user=request.user)
    if request.method == 'GET':
        form = TodoForm(instance=todo)
        return render(request, 'listToDo/viewtodo.html', {'todo': todo, 'form': form})
    else:
        try:
            form = TodoForm(request.POST, instance=todo)
            form.save()
            return redirect('currenttodos')
        except ValueError:
            return render(request, 'listToDo/viewtodo.html', {'todo': todo, 'form': form, 'error': 'Bad data passed in or '
                                                                                             'you\'re not login. Try '
                                                                                             'again.'})

@login_required
def createtodo(request):
    if request.method == 'GET':
        return render(request, 'listToDo/createtodo.html', {'form': TodoForm()})
    else:
        try:
            form = TodoForm(request.POST)
            newtodo = form.save(commit=False)
            newtodo.user = request.user
            newtodo.save()
            return redirect('currenttodos')
        except ValueError:
            return render(request, 'listToDo/createtodo.html', {'form': TodoForm(), 'error': 'Bad data passed in or '
                                                                                             'you\'re not login. Try '
                                                                                             'again.'})

@login_required
def completedtodos(request):
    todos = ToDo.objects.filter(user=request.user, completed__isnull=False)
    return render(request, 'listToDo/completedtodo.html', {'todos': todos})

@login_required
def completedtodo(request, todo_pk):
    todo = get_object_or_404(ToDo, pk=todo_pk, user=request.user)
    if request.method == 'POST':
        todo.completed = timezone.now()
        todo.save()
        return redirect('currenttodos')

@login_required
def deletetodo(request, todo_pk):
    todo = get_object_or_404(ToDo, pk=todo_pk, user=request.user)
    if request.method == 'POST':
        todo.completed = timezone.now()
        todo.delete()
        return redirect('currenttodos')


@login_required
def logoutuser(request):
    if request.method == 'POST':
        logout(request)
        return redirect('home')


def my_custom_page_not_found_view(request, exception):
    return render(request, 'listToDo/errors/404.html')


def my_custom_error_view(request):
    return render(request, 'listToDo/errors/500.html')


def my_custom_permission_denied_view(request, exception):
    return render(request, 'listToDo/errors/403.html')


def my_custom_bad_request_view(request, exception):
    return render(request, 'listToDo/errors/400.html')
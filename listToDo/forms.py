from django.forms import ModelForm
from listToDo.models import ToDo


class TodoForm(ModelForm):
    class Meta:
        model = ToDo
        fields = ['title', 'description', 'priority']